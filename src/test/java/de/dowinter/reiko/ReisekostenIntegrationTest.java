package de.dowinter.reiko;

import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.wildfly.swarm.arquillian.DefaultDeployment;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.*;

@RunWith(Arquillian.class)
@DefaultDeployment(type = DefaultDeployment.Type.WAR, testable = false)
public class ReisekostenIntegrationTest {

    @Test
    @RunAsClient
    public void demoReisekostenAreReturnedWithExpectedValues() {
        when()
                .get("reisekosten/demo")
                .then()
                .statusCode(200)
                .body(
                        "id", equalTo(35),
                        "erwartetBrutto", equalTo(265.43f),
                        "erwartetZuschlag", equalTo(150.0f),
                        "zeitraumAb", equalTo("2018-07-20"),
                        "zeitraumBis", equalTo("2018-07-21"),
                        "einreichDatum", notNullValue());
    }

    @Test
    @RunAsClient
    public void addAndRetrieveReisekosten() {
        final String testDate1 = "2018-05-06";
        final String testDate2 = "2018-05-07";

        given().
                contentType("application/json").
                body("{ \"einreichDatum\": \"" + testDate1 + "\"}").
                when().
                post("reisekosten").
                then().
                statusCode(200);

        when().
                get("reisekosten").
                then().
                body("", hasSize(1), "get(0).einreichDatum", equalTo(testDate1), "get(0).id", notNullValue());

        given().
                contentType("application/json").
                body("{ \"einreichDatum\": \"" + testDate2 + "\"}").
                when().
                post("reisekosten").
                then().
                statusCode(200);

        when().
                get("reisekosten").
                then().
                body("", hasSize(2), "get(1).einreichDatum", equalTo(testDate2), "get(1).id", notNullValue());
    }
}
