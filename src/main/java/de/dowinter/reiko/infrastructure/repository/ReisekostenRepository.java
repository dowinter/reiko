package de.dowinter.reiko.infrastructure.repository;

import de.dowinter.reiko.domain.model.Reisekosten;
import de.dowinter.reiko.infrastructure.entity.ReisekostenEntity;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

@RequestScoped
public class ReisekostenRepository {

    @PersistenceContext
    private EntityManager em;

    public ReisekostenRepository() {
    }

    /**
     * Persists given Reisekosten after converting domain model to Entity.
     * @param re Reisekosten
     */
    public void addReisekosten(Reisekosten re) {
        em.persist(this.convertReisekostenToReisekostenEntity(re));
    }

    /**
     * Updates values of given Reisekosten in database after converting domain model to Entity.
     * @param re Reisekosten
     */
    public void updateReisekosten(Reisekosten re) {
        em.merge(this.convertReisekostenToReisekostenEntity(re));
    }

    /**
     * Finds all persisted ReisekostenEntities and returns them as Reisekosten domain models.
     * @return List of Reisekosten
     */
    public List<Reisekosten> findAllReisekosten() {
        return em.createQuery("SELECT re FROM ReisekostenEntity re", ReisekostenEntity.class)
                .getResultList()
                .stream()
                .map(this::convertReisekostenEntityToReisekosten)
                .collect(Collectors.toList());
    }

    /**
     * Convert ReisekostenEntity into Domain model Reisekosten.
     * @param re ReisekostenEntity
     * @return Reisekosten domain model
     */
    private Reisekosten convertReisekostenEntityToReisekosten(ReisekostenEntity re) {
        return new Reisekosten(
                re.getReisekostenId(),
                re.getZeitraumAb(),
                re.getZeitraumBis(),
                re.getEinreichDatum(),
                re.getErwartetBrutto(),
                re.getErwartetZuschlag()
        );
    }

    /**
     * Convert Reisekosten Domain Model into Reisekosten Entity.
     * @param rk Reisekosten domain model
     * @return ReisekostenEntity
     */
    private ReisekostenEntity convertReisekostenToReisekostenEntity(Reisekosten rk) {
        return new ReisekostenEntity(
                rk.getId(),
                rk.getZeitraumAb(),
                rk.getZeitraumBis(),
                rk.getEinreichDatum(),
                rk.getErwartetBrutto(),
                rk.getErwartetZuschlag()
        );
    }
}
