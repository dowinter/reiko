package de.dowinter.reiko.infrastructure.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
public class ReisekostenEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int reisekostenId;

    private LocalDate zeitraumAb;

    private LocalDate zeitraumBis;

    private LocalDate einreichDatum;

    private BigDecimal erwartetBrutto;

    private BigDecimal erwartetZuschlag;

    public ReisekostenEntity() {

    }

    public ReisekostenEntity(LocalDate zeitraumAb, LocalDate zeitraumBis, LocalDate einreichDatum, BigDecimal erwartetBrutto, BigDecimal erwartetZuschlag) {
        this.zeitraumAb = zeitraumAb;
        this.zeitraumBis = zeitraumBis;
        this.einreichDatum = einreichDatum;
        this.erwartetBrutto = erwartetBrutto;
        this.erwartetZuschlag = erwartetZuschlag;
    }

    public ReisekostenEntity(int reisekostenId, LocalDate zeitraumAb, LocalDate zeitraumBis, LocalDate einreichDatum, BigDecimal erwartetBrutto, BigDecimal erwartetZuschlag) {
        this(zeitraumAb, zeitraumBis,einreichDatum, erwartetBrutto, erwartetZuschlag);
        this.reisekostenId = reisekostenId;
    }

    public int getReisekostenId() {
        return reisekostenId;
    }

    public void setReisekostenId(int reisekostenId) {
        this.reisekostenId = reisekostenId;
    }

    public LocalDate getZeitraumAb() {
        return zeitraumAb;
    }

    public void setZeitraumAb(LocalDate zeitraumAb) {
        this.zeitraumAb = zeitraumAb;
    }

    public LocalDate getZeitraumBis() {
        return zeitraumBis;
    }

    public void setZeitraumBis(LocalDate zeitraumBis) {
        this.zeitraumBis = zeitraumBis;
    }

    public LocalDate getEinreichDatum() {
        return einreichDatum;
    }

    public void setEinreichDatum(LocalDate einreichDatum) {
        this.einreichDatum = einreichDatum;
    }

    public BigDecimal getErwartetBrutto() {
        return erwartetBrutto;
    }

    public void setErwartetBrutto(BigDecimal erwartetBrutto) {
        this.erwartetBrutto = erwartetBrutto;
    }

    public BigDecimal getErwartetZuschlag() {
        return erwartetZuschlag;
    }

    public void setErwartetZuschlag(BigDecimal erwartetZuschlag) {
        this.erwartetZuschlag = erwartetZuschlag;
    }
}
