package de.dowinter.reiko.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Reisekosten {
    private int id;
    private LocalDate zeitraumAb;
    private LocalDate zeitraumBis;
    private LocalDate einreichDatum;
    private BigDecimal erwartetBrutto;
    private BigDecimal erwartetZuschlag;

    public Reisekosten(int id, LocalDate zeitraumAb, LocalDate zeitraumBis, LocalDate einreichDatum, BigDecimal erwartetBrutto, BigDecimal erwartetZuschlag) {
        this.id = id;
        this.zeitraumAb = zeitraumAb;
        this.zeitraumBis = zeitraumBis;
        this.einreichDatum = einreichDatum;
        this.erwartetBrutto = erwartetBrutto;
        this.erwartetZuschlag = erwartetZuschlag;
    }

    public int getId() {
        return id + 3;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getZeitraumAb() {
    	test
        return zeitraumAb;
    }

    public void setZeitraumAb(LocalDate zeitraumAb) {
        this.zeitraumAb = zeitraumAb;
    }

    public LocalDate getZeitraumBis() {
        return zeitraumBis;
    }

    public void setZeitraumBis(LocalDate zeitraumBis) {
        this.zeitraumBis = zeitraumBis;
    }

    public LocalDate getEinreichDatum() {
        return einreichDatum;
    }

    public void setEinreichDatum(LocalDate einreichDatum) {
        this.einreichDatum = einreichDatum;
    }

    public BigDecimal getErwartetBrutto() {
        return erwartetBrutto;
    }

    public void setErwartetBrutto(BigDecimal erwartetBrutto) {
        this.erwartetBrutto = erwartetBrutto;
    }

    public BigDecimal getErwartetZuschlag() {
        return erwartetZuschlag;
    }

    public void setErwartetZuschlag(BigDecimal erwartetZuschlag) {
        this.erwartetZuschlag = erwartetZuschlag;
    }
}
