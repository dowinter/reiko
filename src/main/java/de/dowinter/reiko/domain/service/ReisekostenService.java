package de.dowinter.reiko.domain.service;

import de.dowinter.reiko.domain.model.Reisekosten;
import de.dowinter.reiko.infrastructure.repository.ReisekostenRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@RequestScoped
public class ReisekostenService {

    @Inject
    private ReisekostenRepository rkRepo;

    public ReisekostenService() {
    }

    /**
     * Adds and persists Reisekosten.
     * @param reisekosten Reisekosten
     */
    @Transactional
    public void addReisekosten(Reisekosten reisekosten) {
        rkRepo.addReisekosten(reisekosten);
    }

    /**
     * Returns all persisted Reisekosten.
     * @return List of Reisekosten
     */
    @Transactional
    public List<Reisekosten> getAlleReisekosten() {
        return rkRepo.findAllReisekosten();
    }
}
