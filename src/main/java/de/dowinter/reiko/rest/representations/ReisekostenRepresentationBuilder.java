package de.dowinter.reiko.rest.representations;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ReisekostenRepresentationBuilder {
    private int id;
    private LocalDate zeitraumAb;
    private LocalDate zeitraumBis;
    private LocalDate einreichDatum;
    private BigDecimal erwartetBrutto;
    private BigDecimal erwartetZuschlag;

    public ReisekostenRepresentationBuilder setZeitraumAb(LocalDate zeitraumAb) {
        this.zeitraumAb = zeitraumAb;
        return this;
    }

    public ReisekostenRepresentationBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public ReisekostenRepresentationBuilder setZeitraumBis(LocalDate zeitraumBis) {
        this.zeitraumBis = zeitraumBis;
        return this;
    }

    public ReisekostenRepresentationBuilder setEinreichDatum(LocalDate einreichDatum) {
        this.einreichDatum = einreichDatum;
        return this;
    }

    public ReisekostenRepresentationBuilder setErwartetBrutto(BigDecimal erwartetBrutto) {
        this.erwartetBrutto = erwartetBrutto;
        return this;
    }

    public ReisekostenRepresentationBuilder setErwartetZuschlag(BigDecimal erwartetZuschlag) {
        this.erwartetZuschlag = erwartetZuschlag;
        return this;
    }

    public ReisekostenRepresentation createReisekostenRepresentation() {
        return new ReisekostenRepresentation(id, zeitraumAb, zeitraumBis, einreichDatum, erwartetBrutto, erwartetZuschlag);
    }
}