package de.dowinter.reiko.rest.representations;

import java.math.BigDecimal;
import java.time.LocalDate;

public final class ReisekostenRepresentation {
    public int id;
    public LocalDate zeitraumAb;
    public LocalDate zeitraumBis;
    public LocalDate einreichDatum;
    public BigDecimal erwartetBrutto;
    public BigDecimal erwartetZuschlag;

    public ReisekostenRepresentation() {}

    public ReisekostenRepresentation(int id, LocalDate zeitraumAb, LocalDate zeitraumBis, LocalDate einreichDatum, BigDecimal erwartetBrutto, BigDecimal erwartetZuschlag) {
        this.id = id;
        this.zeitraumAb = zeitraumAb;
        this.zeitraumBis = zeitraumBis;
        this.einreichDatum = einreichDatum;
        this.erwartetBrutto = erwartetBrutto;
        this.erwartetZuschlag = erwartetZuschlag;
    }
}
