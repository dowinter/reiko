package de.dowinter.reiko.rest.api;

import de.dowinter.reiko.domain.model.Reisekosten;
import de.dowinter.reiko.domain.service.ReisekostenService;
import de.dowinter.reiko.rest.representations.ReisekostenRepresentation;
import de.dowinter.reiko.rest.representations.ReisekostenRepresentationBuilder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
@Path("/reisekosten")
public class ReisekostenResource {

	@Inject
	private ReisekostenService rkService;

	@GET
	@Path("/demo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDemoReisekosten() {
		ReisekostenRepresentation demo = new ReisekostenRepresentationBuilder()
				.setId(35)
				.setEinreichDatum(LocalDate.now())
				.setZeitraumAb(LocalDate.parse("2018-07-20"))
				.setZeitraumBis(LocalDate.parse("2018-07-21"))
				.setErwartetBrutto(new BigDecimal("265.43"))
				.setErwartetZuschlag(new BigDecimal("150.00"))
				.createReisekostenRepresentation();
		return Response.ok(demo).build();
	}

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllReisekosten() {
		List<ReisekostenRepresentation> allRk =
				this.rkService.getAlleReisekosten()
						.stream()
						.map(this::convertReisekostenToReisekostenRepresentation)
						.collect(Collectors.toList());
		return Response.ok(allRk).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postReisekosten(ReisekostenRepresentation reisekostenRepresentation) {
		rkService.addReisekosten(convertReisekostenRepresentationToReisekosten(reisekostenRepresentation));
		return Response.ok().build();
	}

	private Reisekosten convertReisekostenRepresentationToReisekosten(ReisekostenRepresentation rkRep) {
		return new Reisekosten(
				rkRep.id,
				rkRep.zeitraumAb,
				rkRep.zeitraumBis,
				rkRep.einreichDatum,
				rkRep.erwartetBrutto,
				rkRep.erwartetZuschlag
		);
	}

	private ReisekostenRepresentation convertReisekostenToReisekostenRepresentation(Reisekosten rk) {
		return new ReisekostenRepresentationBuilder()
				.setId(rk.getId())
				.setErwartetZuschlag(rk.getErwartetZuschlag())
				.setErwartetBrutto(rk.getErwartetBrutto())
				.setZeitraumBis(rk.getZeitraumBis())
				.setZeitraumAb(rk.getZeitraumAb())
				.setEinreichDatum(rk.getEinreichDatum())
				.createReisekostenRepresentation();
	}
}
